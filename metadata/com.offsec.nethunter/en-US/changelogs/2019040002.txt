* New duckyconverter
* Fixed context may produce nullpointer warnings
* Improved initial permissions request and root check
* Use asynctask for non-ui thread runtime execution
* Many smaller improvements
