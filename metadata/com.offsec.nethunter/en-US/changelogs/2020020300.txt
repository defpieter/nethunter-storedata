* Add background location support for Android 10
* Considerable re-write by @simonpunk
* KeX manager by @yesimxev
* usbarmory script by @simonpunk
* Adds multi-user functionality to KeX manager - @yesimxev
! Multi-user KeX requires rootfs 2020.1 or newer
