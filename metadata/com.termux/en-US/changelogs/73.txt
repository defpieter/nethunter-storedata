* Fix problems with sharing large terminal transcripts (#1166).
* Export ANDROID_TZDATA_ROOT as that is needed to run am and dalvikvm on Android Q (#1163).
* Make is possible to add functions keys (F1-F12) as extra keys (#1161).
